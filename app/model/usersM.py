from clients.mysql import MysqlClient
import random
from utils import helpers
from controller import validateC

class User():
    def __init__(self, username=None, passwd=None, token=None):
        self.mysql = MysqlClient()
        if (username != None) & (passwd != None):
            self.username = ''.join(e for e in username if e.isalnum())
            self.passwd = ''.join(e for e in passwd if e.isalnum())
            try:
                self.__fetch_user_by_login()
                self.success = True
            except Exception as E:
                print(E)
                self.success = False
        elif token != None:
            self.token = ''.join(e for e in token if e.isalnum())
            try:
                self.__fetch_user_by_token()
                self.success = True
            except Exception as E:
                print(E)
                self.success = False
        else:
            self.success = False
            
    def __fetch_user_by_token(self):
        query = f"""
        select
            username,
            passwd,
            teams_id,
            user_role,
            user_id
        from
            users
        where
            token = "{self.token}"
        """
        self.mysql.cursor.execute(query)
        data = self.mysql.cursor.fetchone()
        self.username = data[0]
        self.passwd = data[1]
        self.teams_id = data[2].split(",")
        self.role = data[3]
        self.id = data[4]

    def __fetch_user_by_login(self):
        query = f"""
        select
            teams_id,
            user_role,
            token,
            user_id
        from
            users
        where
            username = "{self.username}"
            and passwd = "{self.passwd}"
        """
        self.mysql.cursor.execute(query)
        data = self.mysql.cursor.fetchone()
        self.teams_id = data[0].split(",")
        self.role = data[1]
        self.token = data[2]
        self.id = data[3]

    def __convert_ticket_data_json(self, data):
        tickets = {"tickets": []}
        if len(data) < 1:
            return tickets
        for t in data:
            tickets["tickets"].append(
                {
                    "ticket_id": t[0], 
                    "creator_id":t[1],
                    "owner_id": t[2],
                    "state":t[3],
                    "title": t[4],
                    "created_at":t[5],
                    "creator_username":t[6],
                    "owner_username":t[7],
                    "priority":t[8],
                    "team_name":t[9],
                }
            )
        return tickets

    def fetch_tickets(self):
        query = f"""
        select
            ticket_id,
            creator_id,
            owner_id,
            state,
            title,
            created_at,
            creator.username,
            owner.username,
            priority,
            teams.name      
        from
            tickets t 
        inner join
            users owner on t.owner_id = owner.user_id
        inner join
            users creator on t.creator_id = creator.user_id
        inner join
            teams teams on teams.team_id = t.team_id
        """
        if self.role != "admin":
            query += f"""where
            teams_id in ({helpers.string_for_queries(self.teams_id)});
        """
        print(query)
        self.mysql.cursor.execute(query)
        data = self.mysql.cursor.fetchall()
        tickets = self.__convert_ticket_data_json(data)
        return tickets

    def change_user_role(self, new_role, user_id):
        acceptable_roles = [
            "agent",
            "client",
            "admin"
        ]
        if new_role not in acceptable_roles : return False
        query = f"""
        update
            users
        set
            user_role = '{new_role}'
        where
            user_id = {int(user_id)};
        """
        try:
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            self.role = new_role
            return True
        except Exception as E:
            print("Error on usersM:", E)
            return False

    def change_user_team(self, new_teams_id, user_id):
        query = f"""
        update
            users
        set
            teams_id = '{str(new_teams_id)}'
        where
            user_id = {int(user_id)};
        """
        try:
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            self.teams_id = new_teams_id
            return True
        except Exception as E:
            print("Error on usersM:", E)
            return False
                    

def create_user(data):
    mysql = MysqlClient()
    query = f"""
    insert into
    users (
        username,
        passwd,
        teams_id,
        user_role,
        token
    )
    values(
        '{data["username"]}', 
        '{data["passwd"]}', 
        '{data["teams_id"]}',
        '{data["user_role"]}', 
        '{generate_token()}'
    );
    """
    try:
        mysql.cursor.execute(query)
        mysql.db.commit()
        return True
    except Exception as E:
        print("Error on usersM:", E)
        return False
        
# TO-DO
# Função pra renomear usuário (opcional)
# Função pra mudar a senha do usuário (opcional)

def generate_token():
    token = ""
    for _ in range(50):
        token += "%s" % random.randint(0,9)
        token += ""
    return token