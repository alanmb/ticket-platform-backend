from clients.mysql import MysqlClient

class Teams:
    def __init__(self):
        self.mysql = MysqlClient()
        self.__fetch_teams()

    def __fetch_teams(self):
        query = """select team_id, name from teams;"""
        self.mysql.cursor.execute(query)
        teams_data = self.mysql.cursor.fetchall()
        self.teams = {"teams": []}
        for t in teams_data:
            self.teams["teams"].append({"id": t[0], "team":t[1]})

    def rename_team(self, team_id, new_name):
        try:
            team_id = int(team_id)
        except Exception as E:
            print(E)
            return False
        query = f"""
        update
            teams
        set
            name = '{new_name}'
        where
            team_id = {team_id}
        """
        try:
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            return True
        except Exception as E:
            print(E)
            return False
    
    def add_team(self, team_name):
        try:
            query = f"""
            insert into teams (name)
            values ('{team_name}');
            """
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            return True
        except Exception as E:
            print(E)
            return False

    # def remove_team(self, team_id):
    #     try:
    #         query = f"""
    #         DELETE FROM teams
    #         WHERE
    #             team_id = {team_id};
    #         """
    #         self.mysql.cursor.execute(query)
    #         self.mysql.db.commit()
    #         return True
    #     except Exception as E:
    #         print(E)
    #         return False