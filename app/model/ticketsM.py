from clients.mysql import MysqlClient
from model import usersM
import json
from datetime import datetime

class Ticket:
    def __init__(self, id):
        self.mysql = MysqlClient()
        self.id = id
        self.__fetch_ticket()

    def __fetch_ticket(self):
        query = f"""
        select
            owner_id,
            creator_id,
            t.team_id,
            state,
            priority,
            created_at,
            title,
            body,
            notes,
            closed_at,
            creator.username as creator_name,
            owner.username as owner_name,
            teams.name
        from
            tickets t 
        inner join
            users owner on t.owner_id = owner.user_id
        inner join
            users creator on t.creator_id = creator.user_id
        inner join
            teams teams on teams.team_id = t.team_id
        where
            ticket_id = {self.id};
        """
        self.mysql.cursor.execute(query)
        ticket_data = self.mysql.cursor.fetchone()
        self.owner_id = ticket_data[0]
        self.creator_id = ticket_data[1]
        self.team_id = ticket_data[2]
        self.state = ticket_data[3]
        self.priority = ticket_data[4]
        self.created_at = ticket_data[5]
        self.title = ticket_data[6]
        self.body = ticket_data[7]
        self.notes = ticket_data[8]
        self.closed_at = ticket_data[9]
        self.creator_username = ticket_data[10]
        self.owner_username = ticket_data[11]
        self.team_name = ticket_data[12]
        self.__parse_notes()
    
    def set_owner(self, owner_id):
        query = f"""
        update
            tickets
        set
            owner_id = {owner_id}
        where
            ticket_id = {self.id}
        """
        try:
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            return True
        except Exception as E:
            print("Exception on ticketsM.py:", E)

    def set_state(self, state):
        acceptable_states = [
            "Aberto",
            "Em Andamento",
            "Pendente Cliente",
            "Fechado"
        ]
        if state not in acceptable_states : return False
        query = f"""
        update
            tickets
        set
            state = '{state}'
        where
            ticket_id = {self.id}
        """
        try:
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            self.state = state
            return True
        except Exception as E:
            print("Exception on ticketsM.py:", E)


    def set_priority(self, priority):
        acceptable_priorities = [
            "Baixo",
            "Médio",
            "Alto",
            "Crítico"
        ]
        if priority not in acceptable_priorities : return False
        query = f"""
        update
            tickets
        set
            priority = '{priority}'
        where
            ticket_id = {self.id}
        """
        try:
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            self.priority = priority
            return True
        except Exception as E:
            print("Exception on ticketsM.py:", E)

    def add_note(self, token, note):
        user = usersM.User(token=token)
        if str(self.team_id) not in user.teams_id : return False
        if not user.success : return False
        now = datetime.now()
        date = f"{now.year}-{now.month}-{now.day} {now.hour}:{now.minute}:{now.second}"
        date = "%s-%02d-%02d %02d:%02d:%02d" % (
            now.year,
            now.month,
            now.day,
            now.hour,
            now.minute,
            now.second
        )
        if not self.notes:
            self.parsed_notes = [
                {
                    "username": user.username,
                    "note": note,
                    "date": date
                }
            ]
            self.notes = json.dumps(self.parsed_notes, ensure_ascii=False).encode('utf8')
            self.notes = self.notes.decode()
        else:
            self.parsed_notes.append(
                {
                    "username": user.username,
                    "note": note,
                    "date": date
                }
            )
            self.notes = json.dumps(self.parsed_notes, ensure_ascii=False).encode('utf8')
            self.notes = self.notes.decode()
        try:
            query = f"""
            update
                tickets
            set
                notes = '{self.notes}'
            where
                ticket_id = {self.id};
            """
            self.mysql.cursor.execute(query)
            self.mysql.db.commit()
            return True
        except Exception as E:
            print("Error on tickets model:", E)
            return False

    def __parse_notes(self):
        if not self.notes : return False
        self.parsed_notes = json.loads(self.notes)


def create_ticket(creator_id, team_id, state, priority, title, body):
    mysql = MysqlClient()
    now = datetime.now()
    date = f"{now.year}-{now.month}-{now.day} {now.hour}:{now.minute}:{now.second}"
    date = "%s-%02d-%02d %02d:%02d:%02d" % (
        now.year,
        now.month,
        now.day,
        now.hour,
        now.minute,
        now.second
    )
    query = f"""
    insert into tickets(creator_id, team_id, state, priority, created_at, title, body)
    values (
        {creator_id},
        {team_id},
        '{state}',
        '{priority}',
        '{date}',
        '{title}',
        '{body}'
    );"""
    try:
        mysql.cursor.execute(query)
        mysql.db.commit()
        return True
    except Exception as E:
        print("Error on ticket Model", E)
        return False
