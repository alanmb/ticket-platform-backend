from flask import Flask, request
from flask_cors import CORS
from clients.mysql import MysqlClient
from controller import teamsC, userC, ticketsC

app = Flask(__name__)
CORS(app)

# rotas teams ------------------------------------------------------------
@app.route("/api/v1/teams/list_teams", methods=["POST"])
def list_teams():
    return teamsC.get_teams(request.json)

@app.route("/api/v1/teams/add_team", methods=["POST"])
def add_team():
    return teamsC.add_team(request.json)

@app.route("/api/v1/teams/rename_team", methods=["POST"])
def rename_team():
    return teamsC.rename_team(request.json)

# rotas users ------------------------------------------------------------
@app.route("/api/v1/login", methods=["POST"])
def login():
    return userC.login(request.json)

@app.route("/api/v1/user/add_user", methods=["POST"])
def add_user():
    return userC.add_user(request.json)

@app.route("/api/v1/user/change_role", methods=["POST"])
def change_role():
    return userC.change_role(request.json)

@app.route("/api/v1/user/change_team", methods=["POST"])
def change_team():
    return userC.change_team(request.json)

@app.route("/api/v1/user/fetch_tickets", methods=["POST"])
def fetch_tickets():
    return userC.fetch_tickets(request.json)

# rotas tickets ------------------------------------------------------------
@app.route("/api/v1/tickets/fetch_ticket", methods=["POST"])
def fetch_ticket():
    return ticketsC.fetch_ticket(request.json)

@app.route("/api/v1/tickets/set_owner", methods=["POST"])
def set_owner():
    return ticketsC.set_owner(request.json)

@app.route("/api/v1/tickets/set_state", methods=["POST"])
def set_state():
    return ticketsC.set_state(request.json)

@app.route("/api/v1/tickets/set_priority", methods=["POST"])
def set_priority():
    return ticketsC.set_priority(request.json)

@app.route("/api/v1/tickets/create_ticket", methods=["POST"])
def create_ticket():
    return ticketsC.create_ticket(request.json)

if __name__ == '__main__':
    mysql = MysqlClient()
    app.run(debug=False, host='0.0.0.0')