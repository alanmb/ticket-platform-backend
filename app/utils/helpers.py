def string_for_queries(item_list, add_quote=False):
    if add_quote : item_list = ["'%s'" % i for i in item_list]
    s = ""
    for i in item_list:
        s += "%s,\n" % i
    s = s[:-2]
    return s