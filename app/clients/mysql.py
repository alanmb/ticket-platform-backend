from clients.config import mysql_conf
import mysql.connector

class MysqlClient():
    def __init__(self):
        self.hostname = mysql_conf.MYSQL["hostname"]
        self.user = mysql_conf.MYSQL["user"]
        self.password = mysql_conf.MYSQL["password"]
        self.port = mysql_conf.MYSQL["port"]
        self.db = self.__connect_database()
        self.cursor = self.db.cursor()
        try:
            self.cursor.execute("use osticket;")
        except:
            self.__initiate_database()

    def __connect_database(self):
        db = mysql.connector.connect(
            host=self.hostname,
            user=self.user,
            password=self.password,
            port=self.port
        )
        return db

    def __initiate_database(self):
        query = """create database osticket;
        use osticket;

        create table teams (
            team_id int NOT NULL AUTO_INCREMENT,
            name varchar(15) NOT NULL UNIQUE,
            PRIMARY KEY (team_id)
        );

        create table users (
            user_id int NOT NULL AUTO_INCREMENT,
            username varchar(255) NOT NULL UNIQUE,
            passwd varchar(255) NOT NULL,
            teams_id varchar(255) NOT NULL,
            user_role varchar(15) NOT NULL,
            token varchar(50) NOT NULL UNIQUE,
            PRIMARY KEY (user_id)
        );

        create table tickets (
            ticket_id int NOT NULL AUTO_INCREMENT,
            owner_id int DEFAULT NULL,
            creator_id int NOT NULL,
            team_id int DEFAULT NULL,
            state varchar (15) NOT NULL,
            priority varchar (15) NOT NULL,
            created_at DATETIME NOT NULL,
            title varchar(50) NOT NULL,
            body varchar(500) NOT NULL,
            notes LONGTEXT DEFAULT NULL,
            closed_at DATETIME DEFAULT NULL,
            PRIMARY KEY (ticket_id),
            FOREIGN KEY (owner_id) REFERENCES users(user_id),
            FOREIGN KEY (team_id) REFERENCES teams(team_id)
        );


        -- insert admin user
        insert into users (username, passwd, teams_id, user_role, token)
        values ('admin', '321Adm1n123', '1,2,3', 'admin', '02273234414495844850261227330537291158173003975657');

        insert into users (username, passwd, teams_id, user_role, token)
        values ('alan', 'banana123', '2', 'agent', '5050505050505050505050');
        
        -- insert teams
        insert into teams (name)
        values ('SRE'), ('Data Science'), ('Development');
        
        -- insert ticket
        insert into tickets(owner_id, creator_id, team_id, state, priority, created_at, title, body, notes)
        values (1, 1, 1,'Aberto','Baixo','2021-06-20','Sem Permissão na Plataforma','Estou precisando adicionar mais times ao meu usuário','[{"username": "admin", "note": "Bom tarde, alguma atualização?", "date": "2021-06-24 00:14:32"}]');
        
        insert into tickets(owner_id, creator_id, team_id, state, priority, created_at, title, body)
	    values  (2, 1, 2,'Aberto','Alto','2021-06-15','Provisionamento de Plano', 'Provisionem o plano degustação');

        insert into tickets(owner_id, creator_id, team_id, state, priority, created_at, title, body)
        values (2, 2, 2, 'Aberto', 'Baixo', '2021-06-20', 'Model de users','Estou precisando adicionar mais times ao meu usuário');"""
       
        queries = query.split(";")
        try:
            for query in queries:
                print(query)
                self.cursor.execute(query)
                self.db.commit()
        except Exception as E:
            print("Error creating database:", E)
            query = """
            drop database osticket;
            """
            self.cursor.execute(query)
            self.db.commit()