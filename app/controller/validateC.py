from model import usersM
from model import teamsM
from model import ticketsM

def check_team_exists(team_id):
    teams = teamsM.Teams()
    valid_teams_id = [i["id"] for i in teams.teams["teams"]]
    if int(team_id) not in valid_teams_id : return {"Error": "Trying to add inexistent team"}
    return True

def check_admin(token):
    user = usersM.User(token=token)
    if not user.success:
        return {"Error": "Invalid Token"}
    elif user.role != "admin":
        return {"Error": "User not authorized"}
    return True

def check_team_permission(token, ticket_id):
    user = usersM.User(token=token)
    ticket = ticketsM.Ticket(ticket_id)
    if not user.success : return {"Error": "Invalid Token"}
    print(user.teams_id)
    if str(ticket.team_id) not in user.teams_id : return {"Error": "User unauthorized for this ticket."}
    return True