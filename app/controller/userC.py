from model import usersM, teamsM
from controller import validateC

def login(data):
    user = usersM.User(data["username"], data["passwd"])
    if user.success:
        return {'name': user.username, 'teams': user.teams_id, 'role': user.role, 'token': user.token}
    else:
        return {'Error': 'name and password combination was not found.'}

def add_user(data):
    is_admin = validateC.check_admin(data["token"])
    if is_admin != True : return is_admin
    teams_ids = data["teams_id"].split(",")
    for i in teams_ids:
        team_exists = validateC.check_team_exists(i)
        if team_exists != True : return team_exists
    if usersM.create_user(data) : return {f"Success" : "User created!"}
    return {"Error" : "Could not create User"}

def change_role(data):
    is_admin = validateC.check_admin(data["token"])
    if is_admin != True : return is_admin
    user = usersM.User()
    if user.change_user_role(data["new_role"], data["user_id"]) : return {f"Success" : "Role changed!"}
    return {"Error" : "Could not change Role"}

def change_team(data):
    is_admin = validateC.check_admin(data["token"])
    if is_admin != True : return is_admin
    teams_ids = data["new_teams_id"].split(",")
    for i in teams_ids:
        team_exists = validateC.check_team_exists(i)
        if team_exists != True : return team_exists
    user = usersM.User()
    if user.change_user_team(data["new_teams_id"], data["user_id"]) : return {f"Success" : "Team changed!"}
    return {"Error" : "Could not change Team"}

def fetch_tickets(data):
    user = usersM.User(token=data["token"])
    tickets = user.fetch_tickets()
    if tickets == None : return {"Error" : "Could not bring tickets"}
    return tickets