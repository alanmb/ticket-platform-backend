from clients.mysql import MysqlClient
from model import usersM
from model import teamsM
from controller import validateC

def get_teams(data):
    is_admin = validateC.check_admin(data["token"])
    if is_admin != True : return is_admin
    teams = teamsM.Teams()
    return teams.teams

def add_team(data):
    is_admin = validateC.check_admin(data["token"])
    if is_admin != True : return is_admin
    teams = teamsM.Teams()
    if teams.add_team(data["team_name"]) : return {f"Success": "Team created"}
    return {"Error": "Could not create Team"}

def rename_team(data):
    is_admin = validateC.check_admin(data["token"])
    if is_admin != True : return is_admin
    teams = teamsM.Teams()
    if teams.rename_team(data["team_id"], data["new_name"]) : return {f"Success": "Team renamed!"}
    return {"Error": "Could not rename Team"}

# def delete_team(data):
#     user = usersM.User(token=data["token"])
#     if not user.success : return {"Error": "User not authorized"}
#     teams = teamsM.Teams()
#     return teams.teams